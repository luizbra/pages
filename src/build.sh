#!/bin/sh -e

BASEDIR="$(dirname "$(realpath "$0")")"
TEMPLATE="$BASEDIR/template.html"
POSTS="$BASEDIR/posts"
DESTDIR="$BASEDIR/.."
BASETITLE="luizbra.dev"

while getopts ':i:o:t:T:' opt; do
    case "$opt" in
        i) POSTS="$OPTARG" ;;
        o) DESTDIR="$OPTARG" ;;
        t) TEMPLATE="$OPTARG" ;;
        T) BASETITLE="$OPTARG" ;;
        *) echo "usage: $(basename "$0") [-i src_posts] [-o dest] [-t template] [-T title]"; exit 1
    esac
done

markdown --version 1> /dev/null || exit 1
mkdir -p "$DESTDIR"
[ -d "$DESTDIR/posts" ] && rm -r "$DESTDIR/posts"
mkdir -p "$DESTDIR/posts"
TEMPLATE_HEAD="$(sed -n '/<!-- CONTENT -->/!p;//q' "$TEMPLATE")"
TEMPLATE_TAIL="$(sed '1,/<!-- CONTENT -->/d' "$TEMPLATE")"

{
    echo "$TEMPLATE_HEAD" | sed "s/<!-- TITLE -->/$BASETITLE/"
    printf '<h1>Tutorials</h1>\n<ul>\n'
    find "$POSTS" -name '*.md' | sort -r | while read -r post; do
        filename="$(basename "$post" .md)"
        title="$(sed -n '1s/^# //p' "$post")"
        {
            echo "$TEMPLATE_HEAD" | sed "s/<!-- TITLE -->/$BASETITLE - $title/"
            markdown "$post"
            echo "$TEMPLATE_TAIL"
        } > "$DESTDIR/posts/$filename.html"
        echo "<li><a href=\"posts/$filename.html\">$title</a></li>"
    done
    echo '</ul>'
    echo "$TEMPLATE_TAIL"
} > "$DESTDIR/index.html"
