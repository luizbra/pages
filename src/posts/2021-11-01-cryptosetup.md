# How to encrypt external drives on Linux

This tutorial describes how to perform full disk encryption, *partitionless*, on a external block device using [Cryptsetup](https://gitlab.com/cryptsetup/cryptsetup).

## Install Cryptsetup

    apt-get install cryptsetup libblockdev-crypto2

## Optional: wipe the disk

From Cryptsetup's FAQ:

> If the target was in use previously, it is a good idea to wipe it before creating the LUKS container in order to remove any trace of old file systems and data.  For example, some users have managed to run e2fsck on a partition containing a LUKS container, possibly because of residual ext2 superblocks from an earlier use.  This can do arbitrary damage up to complete and permanent loss of all data in the LUKS container.

    cryptsetup open --type plain -d /dev/urandom /dev/sdX to_be_wiped
    dd if=/dev/zero of=/dev/mapper/to_be_wiped status=progress
    cryptsetup close to_be_wiped

## Format the disk

    cryptsetup luksFormat /dev/sdX
    cryptsetup open /dev/sdX to_be_formated
    mkfs.ext4 /dev/mapper/to_be_formated
    cryptsetup close to_be_formated

## Usage

Mount the drive:

    cryptsetup open /dev/sdX crypt
    mount /dev/mapper/crypt /mnt

Unmount:

    umount /mnt
    cryptsetup close crypt
