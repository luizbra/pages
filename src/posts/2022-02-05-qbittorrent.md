# qbittorrent - Never enter a torrent website again

Torrents allows you to send and receive files freely and quickly (peer-to-peer model). To use this technology you'll need a BitTorrent client, the best, undoubtedly, is [qBittorrent](https://www.qbittorrent.org/).

## qBittorrent Anonymous Mode

> WARNING: anonymous mode doesn't provide strong privacy guarantees on its own. If you are concerned about legal authorities and copyright trouble, for example, consider using a VPN instead (or in addition to it). Anonymous mode is only meant to further prevent your BitTorrent traffic from being associated to you, even when using other privacy enhancing mechanisms (such as a VPN service), by limiting the scope of the information broadcasted by the client (such as the fingerprint in the peer-ID).

When enabled, the peer-ID will no longer include the client's fingerprint, the user-agent will be set to an empty string, other identifying information (IP, listening port, etc.) will not be exposed to the public.

## qBittorrent's BEST feature

qBittorrent counts with a meta search engine that allows you to search throughout torrent websites without being exposed to Busty Asian Beauties™ at every click. On the "View" menu check the "Search Engine" box, then, a new tab will appear, in "Search plugins" (bottom left corner) you can manage all of the search engines. When adding new search engines be careful to not get plugins that are incompatible with your package's version.

![Example](/assets/img/qbittorrent.jpeg)
